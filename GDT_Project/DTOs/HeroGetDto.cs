﻿using GDT_Project.Models.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.DTOs
{
    public class HeroGetDto
    {
        public int HeroId { get; set; }
        public string HeroName { get; set; }
        public int Grade { get; set; }
        public int Level { get; set; } = 1;//set default value
    }
}