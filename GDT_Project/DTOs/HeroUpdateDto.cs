﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.DTOs
{
    public class HeroUpdateDto
    {
        public string HeroName { get; set; }
        public int Grade { get; set; }
        public int Level { get; set; }
    }
}