﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GDT_Project.Entities;

namespace GDT_Project.Services
{
    public interface IAuthService
    {
        Task<ResponseService<int> >Register(User user,string password);
        Task<ResponseService<string>>Login(string username,string password);
        Task<bool>UserExists(string username);

    }
}
