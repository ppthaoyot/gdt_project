﻿using GDT_Project.DTOs;
using GDT_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public interface IHeroService
    {
        Task<ResponseService<List<HeroGetDto>>> GetHero();

        Task<ResponseService<List<HeroGetDto>>> GetAllHeroes(PaginationDto pagination);

        Task<ResponseService<HeroGetDto>> GetHeroById(int id);

        Task<ResponseService<List<HeroGetDto>>> AddHero(HeroAddDto newHero);

        Task<ResponseService<HeroGetDto>> UpdateHero(int heroId, HeroUpdateDto updateDto);

        Task<ResponseService<List<HeroGetDto>>> DeleteHero(int heroId);
    }
}