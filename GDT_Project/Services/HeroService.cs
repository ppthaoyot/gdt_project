﻿using AutoMapper;
using GDT_Project.DTOs;
using GDT_Project.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    //implement interface
    public class HeroService : IHeroService

    {
        //constructor
        private List<Hero> _heroes;

        private readonly IMapper _mapper;

        //constructor
        public HeroService(IMapper mapper)
        {
            List<Hero> heroes = new List<Hero>
            {
                new Hero(){ HeroId =1,HeroName="Mewnich",Grade=1,Level=0 },
                new Hero(){ HeroId =2,HeroName="MinMin",Grade=1,Level=1 },
                new Hero(){ HeroId =3,HeroName="Cherprang",Grade=1,Level=2 },
                new Hero(){ HeroId =4,HeroName="Aom",Grade=1,Level=3 },
            };

            _heroes = heroes;
            this._mapper = mapper;
        }

        //เพิ่ม async ทุกครั้ง
        public async Task<ResponseService<List<HeroGetDto>>> GetHero()
        {
            var response = new ResponseService<List<HeroGetDto>>();

            //auto mapper
            var heroDto = _mapper.Map<List<HeroGetDto>>(_heroes);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponseService<HeroGetDto>> GetHeroById(int id)
        {
            var response = new ResponseService<HeroGetDto>();
            var hero = _heroes.FirstOrDefault(x => x.HeroId.Equals(id));

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this hero id {id} not found.";

                return response;
            }
            else
            {
                //auto mapper
                var heroDto = _mapper.Map<HeroGetDto>(hero);
                response.Data = heroDto;
            }

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> AddHero(HeroAddDto newHero)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            try
            {
                var hero = _mapper.Map<Hero>(newHero);

                hero.HeroId = _heroes.Max(x => x.HeroId + 1);

                _heroes.Add(hero);

                var heroDto = _mapper.Map<List<HeroGetDto>>(_heroes);

                response.Data = heroDto;
            }
            catch (Exception e)
            {
                response.IsSuccess = false;
                response.Message = e.Message;
            }

            return response;
        }

        public async Task<ResponseService<HeroGetDto>> UpdateHero(int heroId, HeroUpdateDto updateDto)
        {
            var response = new ResponseService<HeroGetDto>();

            var hero = _heroes.FirstOrDefault(x => x.HeroId == heroId);

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = "update fail.";
                return response;
            }

            hero.HeroName = updateDto.HeroName;
            hero.Grade = updateDto.Grade;

            var heroDto = _mapper.Map<HeroGetDto>(hero);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> DeleteHero(int heroId)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            var hero = _heroes.FirstOrDefault(x => x.HeroId == heroId);

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = "delete fail / not found hero";
                return response;
            }
            _heroes.Remove(hero);

            var heroes = _mapper.Map<List<HeroGetDto>>(_heroes);

            response.Data = heroes;

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> GetAllHeroes(PaginationDto pagination)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            //auto mapper
            var heroDto = _mapper.Map<List<HeroGetDto>>(_heroes);

            response.Data = heroDto;

            return response;
        }
    }
}