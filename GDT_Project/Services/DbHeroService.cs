﻿using AutoMapper;
using GDT_Project.Data;
using GDT_Project.DTOs;
using GDT_Project.Entities;
using GDT_Project.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public class DbHeroService : IHeroService
    {
        private readonly GDTContext _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        //Depedency injection
        public DbHeroService(GDTContext context, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            this._context = context;
            this._mapper = mapper;
            this._httpContextAccessor = httpContextAccessor;
        }

        public async Task<ResponseService<List<HeroGetDto>>> AddHero(HeroAddDto newHero)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            try
            {
                var hero = _mapper.Map<Hero>(newHero);

                _context.Hero.Add(hero);
                await _context.SaveChangesAsync();

                var heroes = await _context.Hero.AsNoTracking().ToListAsync();

                var heroDto = _mapper.Map<List<HeroGetDto>>(heroes);

                response.Data = heroDto;
            }
            catch (Exception e)
            {
                response.IsSuccess = false;
                response.Message = e.Message;
            }

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> DeleteHero(int heroId)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            var hero = await _context.Hero.FirstOrDefaultAsync(x => x.HeroId == heroId);

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = "delete fail / not found hero";
                return response;
            }
            _context.Remove(hero);
            await _context.SaveChangesAsync();

            var heroeslist = await _context.Hero.AsNoTracking().ToListAsync();
            var heroes = _mapper.Map<List<HeroGetDto>>(heroeslist);

            response.Data = heroes;

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> GetAllHeroes(PaginationDto pagination)
        {
            var response = new ResponseService<List<HeroGetDto>>();

            var queryable = _context.Hero.AsQueryable();
            await _httpContextAccessor.HttpContext.InsertPaginationParametersInResponse(queryable, pagination.RecordsPerPage, queryable.Count(), pagination.Page);
            var lstHeroes = await queryable.Paginate(pagination).ToListAsync();

            //var heroes = await _context.Hero.AsNoTracking().ToListAsync();
            //auto mapper
            var heroDto = _mapper.Map<List<HeroGetDto>>(lstHeroes);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponseService<List<HeroGetDto>>> GetHero()
        {
            var response = new ResponseService<List<HeroGetDto>>();

            var heroes = await _context.Hero.AsNoTracking().ToListAsync();
            //auto mapper
            var heroDto = _mapper.Map<List<HeroGetDto>>(heroes);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponseService<HeroGetDto>> GetHeroById(int id)
        {
            var response = new ResponseService<HeroGetDto>();
            var hero = await _context.Hero.AsNoTracking().FirstOrDefaultAsync(x => x.HeroId.Equals(id));

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this hero id {id} not found.";

                return response;
            }
            else
            {
                //auto mapper
                var heroDto = _mapper.Map<HeroGetDto>(hero);
                response.Data = heroDto;
            }

            return response;
        }

        public async Task<ResponseService<HeroGetDto>> UpdateHero(int heroId, HeroUpdateDto updateDto)
        {
            var response = new ResponseService<HeroGetDto>();

            Hero hero = await _context.Hero.FirstOrDefaultAsync(x => x.HeroId == heroId);

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = "update fail.";
                return response;
            }

            hero.HeroName = updateDto.HeroName;
            hero.Grade = updateDto.Grade;
            hero.Level = updateDto.Level;

            //_context.Hero.Update(hero);
            await _context.SaveChangesAsync();

            var heroDto = _mapper.Map<HeroGetDto>(hero);

            response.Data = heroDto;

            return response;
        }
    }
}