﻿using GDT_Project.DTOs;
using GDT_Project.Models;
using GDT_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace GDT_Project.Controllers
{
    //ถ้ายังไม่ได้ Authorize ให้ comment ส่วนนี้ไปก่อน
    [Authorize]
    [ApiController]
    [Route("hero")]
    public class HeroController : ControllerBase
    {
        //assign field
        private readonly IHeroService _heroService;

        //Dependency Injection(DI)
        public HeroController(IHeroService heroService)
        {
            this._heroService = heroService;
        }

        [HttpGet]
        [Route("getallheroes")]
        public async Task<IActionResult> GetAllHeroes([FromQuery] PaginationDto pagination)
        {
            var response = await _heroService.GetAllHeroes(pagination);

            return Ok(response);
        }

        [HttpGet]
        [Route("getall")]
        [ResponseCache(Duration = 30)]
        public async Task<IActionResult> GetHero()
        {
            var response = await _heroService.GetHero();

            return Ok(response);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetHeroById(int id)
        {
            var response = await _heroService.GetHeroById(id);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpPost("addhero")]
        public async Task<IActionResult> AddHero(HeroAddDto hero)
        {
            var response = await _heroService.AddHero(hero);

            return Ok(response);
        }

        [HttpPut("updatehero/{heroId}")]
        public async Task<IActionResult> UpdateHero(int heroId, HeroUpdateDto hero)
        {
            var response = await _heroService.UpdateHero(heroId, hero);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }

        [HttpDelete("deletehero/{heroId}")]
        public async Task<IActionResult> UpdateHero(int heroId)
        {
            var response = await _heroService.DeleteHero(heroId);

            if (!response.IsSuccess)
            {
                return NotFound(response);
            }

            return Ok(response);
        }
    }
}