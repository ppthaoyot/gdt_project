﻿using GDT_Project.DTOs;
using GDT_Project.Entities;
using GDT_Project.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            this._authService = authService;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(UserRegisterDto registerDto)
        {
            var response = await _authService.Register(
                new User { UserName = registerDto.Username }, registerDto.Password);

            if (!response.IsSuccess)
            {
                return BadRequest(response);
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(UserLoginDto loginDto)
        {
            var response = await _authService.Login(loginDto.Username, loginDto.Password);

            if (!response.IsSuccess)
            {
                return BadRequest(response);
            }

            //return OK status 200
            return Ok(response);
        }
    }
}